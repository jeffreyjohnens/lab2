0. Clone the repo.
	$ git clone https://gitlab.com/jeffreyjohnens/lab2

1. Create a main.cpp file that does the following.
	- reads an array from input (ex. 4 5 6 6 10\n)
	- prints the array
	- sorts the array
	- prints the sorted array

2. Compile and run
	$ g++ -o main main.cpp
	$ ./main 40 50 60 13

3. For the above example the output should look like
	$ ./main 40 50 60 13
	array : [40, 50, 60, 13]
	sorted : [13, 40, 50, 60]

4. Run the test script. Show me the output and your main.cpp
	$ sh test.sh
